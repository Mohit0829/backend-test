<aside class="main-sidebar">
    <section class="sidebar">

        <ul class="sidebar-menu" data-widget="tree">

            <li class="{{ mv_activate_menu(route('backend.dashboard')) }}">
                <a href="{{ route('backend.dashboard') }}">
                    <i class="fa fa-tachometer"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="{{ mv_activate_menu(route('backend.employee.index')) }}">
                <a href="{{ route('backend.employee.index') }}">
                    <i class="fa fa-tachometer"></i>
                    <span>Employees</span>
                </a>
            </li>

        </ul>

    </section>
</aside>
