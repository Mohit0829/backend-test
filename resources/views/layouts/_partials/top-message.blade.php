@if ($errors->any())
    <mv-alert message="{!! implode("<br>", $errors->all()); !!}" type="danger"></mv-alert>
@endif

@if(session()->has('message'))
    <mv-alert message="{{ session('message') }}"
              type="{{ session()->has('message_type') ? session('message_type') : 'success' }}"></mv-alert>
@endif