<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->


    <!-- Styles -->
    <link href="{{ asset('backend/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('tel/css/intlTelInput.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .gallery-delete-btn{
            position: absolute;
            top: 40%;
            left: 50%;
            display: none;

        }

        .img-preview-container:hover .gallery-delete-btn {
            display: block;
        }

        .iti-flag {
            background-image: url("https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/10.0.2/img/flags.png");
        }

        .error{
            color:red;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini {{ Auth::guest() ? 'auth-page' : '' }}">
<div id="app">
    @if(Auth::check())
        <div class="wrapper">
            @include('layouts._partials.header')
            @include('layouts._partials.sidebar')
            <div class="content-wrapper">
                <section class="content">
                    @include('layouts._partials.top-message')
                    @include('layouts._partials.contentheader')
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-solid @stack('box-wrapper')">
                                <div class="box-body @stack('box-body')">
                                    @yield('content')

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            @include('layouts._partials.footer')
        </div>

    @else
        @yield('content')

    @endif

</div>
<script src="{{ asset('backend/js/app.js') }}"></script>

<script src="https://cdn.tiny.cloud/1/bgoa30ctk6ynobtyaud7cnuqpju2tog6m32q0jkd7nws3vrs/tinymce/5/tinymce.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script src="{{ asset('tel/js/intlTelInput.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="{{ asset('tel/js/intlTelInput-jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha256-bqVeqGdJ7h/lYPq6xrPv/YGzMEb6dNxlfiTUHSgRCp8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha256-+BEKmIvQ6IsL8sHcvidtDrNOdZO3C9LtFPtF2H0dOHI=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js" integrity="sha256-dppmU3M7PmToUPE0IZQEFK+v6GJaz5YzVOZN+uxRiDw=" crossorigin="anonymous"></script>

@stack('scripts')
</body>
</html>
