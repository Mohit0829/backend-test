<footer>
    <div class="privacy_policy">
        <p>Copyright &copy; 2019 Roy Kapur Films</p>
        <!--
                <a class="rem_link_style">Privacy Policy</a> |
                <a class="rem_link_style">Disclaimer</a>
        -->
    </div>
    <div class="social_icons">
        <a class="social_icon_items" target="_blank" href="https://www.facebook.com/RoyKapurFilms/">
            <i class="fab fa-facebook-f"></i>
        </a>
        <a class="social_icon_items" target="_blank" href="https://in.linkedin.com/company/roy-kapur-films">
            <i class="fab fa-linkedin-in"></i>
        </a>
        <a class="social_icon_items" target="_blank" href="https://twitter.com/roykapurfilms">
            <i class="fab fa-twitter"></i>
        </a>
        <a class="social_icon_items" target="_blank" href="https://www.instagram.com/roykapurfilms/">
            <i class="fab fa-instagram"></i>
        </a>
        <a class="social_icon_items" target="_blank" href="https://www.youtube.com/channel/UCFJPddB7fRbjdtqMSqnuFcg">
            <i class="fab fa-youtube"></i>
        </a>
    </div>
    <div class="copyrights">
        <p>
            Designed and Developed by <a href="http://www.ting.in/" class="rem_link_style ting_style">ting</a></p>
    </div>
</footer>
