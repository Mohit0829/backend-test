<DOCTYPE html>
<html class="no-js" lang="">
<head>

@include('layouts/frontend/include_css')
@yield('title')

</head>


<body>

@include('layouts/frontend/browser_upgrade')
@include('layouts/frontend/loader')
@include('layouts/frontend/header')

 @yield('content')

@include('layouts/frontend/footer')
@include('layouts/frontend/include_js')

</body>

</html>
