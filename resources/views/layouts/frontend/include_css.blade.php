<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="description" content="">

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<meta name="theme-color" content="#e20014" />

<meta property="og:title" content="Roy Kapur Films" />
<meta property="og:description" content="Siddharth’s filmography as a producer includes some of the biggest blockbusters as well as some of the most acclaimed and path-breaking films of Indian cinema." />
<meta property="og:url" content="https://roykapurfilms.com" />
<meta property="og:type" content="website" />
<meta property="og:image" content="https://roykapurfilms.com/dist/img/og-image.jpg" />
<meta property="og:image:secure_url" content="https://roykapurfilms.com/dist/img/og-image.jpg" />

<title>Roy Kapur Films</title>

<link rel="shortcut icon" type="image/x-icon" href="{{ asset('dist/img/favicon.ico') }}" />

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('dist/css/app.min.css') }}" />

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130566299-43"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-130566299-43');
</script>

