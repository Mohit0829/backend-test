@extends('layouts.app')

@section('contentheader_title')
    EMPLOYEE
@endsection


 @section('content')
        <form action="{{ route('backend.employee.store')}}" method="post" enctype="multipart/form-data" id="form-data">
          @csrf

            <div class="form-group" style="position: relative;overflow: hidden;">
                <label for="feature_image">Employee Image</label>
               <input id="feature_image" type="file" name="feature_image">
            </div>

             <div class="form-group">
               <label for="name">Employee Name</label>
               <input type="text" name="employee_name" id="employee_name" class="form-control" value="" required="required">
             </div>

             <div class="form-group">
              <label for="front_year">Employee ID</label>
              <input type="text" name="employee_id" id="employee_id" class="form-control" minlength="5" maxlength="5" value="" required="required">
             </div>

            <div class="form-group">
                <label for="front_year">Date of Birth</label>
                <input type="text" name="birth" id="birth" class="form-control" value="" readonly="readonly">
            </div>

              <div class="form-group">
                <label for="description">Email ID</label>
                <input type="email" name="email_id" id="email_id" class="form-control" value="" required="required">
              </div>

              <div class="form-group">
              <label for="release_date">Mobile</label>
              <input type="text" name="mobile" id="mobile" class="form-control" value="" minlength="10" maxlength="10">
              <span id="valid-msg" class="hide">✓ Valid</span>
              <span id="error-msg" class="hide"></span>
             </div>

            <h5 class="text-right">
              <button class="btn btn-primary btn-md">SAVE</button>
              <button type="reset" class="btn btn-primary btn-md">CANCEL</button>
            </h5>
        </form>
@endsection

@push('scripts')
    <script>
tinymce.init({
selector: '.dynamic-editor'
});

$("#mobile").intlTelInput({
    nationalMode: true,
    // initialCountry: "India",
});

var input = document.querySelector("#mobile"),
  errorMsg = document.querySelector("#error-msg"),
  validMsg = document.querySelector("#valid-msg");

// here, the index maps to the error code returned from getValidationError - see readme
var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

// initialise plugin
var iti = window.intlTelInput(input, {
  utilsScript: "{{ asset('tel/js/utils.js') }}"
});

var reset = function() {
  input.classList.remove("error");
  errorMsg.innerHTML = "";
  errorMsg.classList.add("hide");
  validMsg.classList.add("hide");
};

// on blur: validate
input.addEventListener('blur', function() {
  reset();
  if (input.value.trim()) {
    if (iti.isValidNumber()) {
      validMsg.classList.remove("hide");
      return true;
    } else {
      input.classList.add("error");
      var errorCode = iti.getValidationError();
      errorMsg.innerHTML = errorMap[errorCode];
      errorMsg.classList.remove("hide");
      return false;
    }
  }
});

// on keyup / change flag: reset
input.addEventListener('change', reset);
input.addEventListener('keyup', reset);


var input = document.querySelector("#mobile");
window.intlTelInput(input, {
  hiddenInput: "full_phone",
  utilsScript: "{{ asset('tel/js/utils.js') }}" // just for formatting/placeholders etc
});

$('#birth').datepicker({
    format: 'dd-mm-yyyy',
});

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only please"); 

$('#form-data').validate({
    errorElement: "span",
    rules: {
        employee_name: {
          required: true,
          lettersonly: true,
        },
        email_id: {
            required: true,
            email: true,
        },
        feature_image: {
            extension: "jpg|png|jpeg",
        },
        employee_id: {
            required: true,
            minlength: 5,
            maxlength: 5,
        },
        mobile: {
            minlength: 10,
            maxlength: 10,
        },
    },
    messages: {
      required: "This field is required",
      lettersonly: "Only alphabetic characters is valid",
      email: "Please enter a valid email address",
      extension: "Your image must be in valid format",
    },
    submitHandler: function(form) {
    // do other things for a valid form
    form.submit();
    }
});

function deleteGalleryByUrl(url, type, elem) {
    var req_data = {_token: $('meta[name="csrf-token"]').attr('content')};
    $.ajax({
        url: url,
        type: type,
        data: req_data,
        success: function (response) {
            $(elem).parents(".img-rem").remove();
        }
    });
}


function updateGalleryByUrl(url, type, elem) {
    var img_data = $(elem).parents('.position').find('input[name="img-position"]').val();
    var req_data = {'img_position': img_data, _token: $('meta[name="csrf-token"]').attr('content')};
    console.log(req_data);
    $.ajax({
        url: url,
        type: type,
        data: req_data,
        success: function (response) {
            alert('Position Updated');
//                                            $(elem).parents(".img-rem").remove();
            // swal(response);
            swal("Poof! Your imaginary file has been updated!", {
                title: "Position Updated",
                icon: "success"
            });
        }
    });
}


function deleteBTSByUrl(url, type, elem) {
    var req_data = {_token: $('meta[name="csrf-token"]').attr('content')};
    $.ajax({
        url: url,
        type: type,
        data: req_data,
        success: function (response) {
            $(elem).parents(".img-rem").remove();
        }
    });
}


function updateBTSByUrl(url, type, elem) {
    var img_data = $(elem).parents('.position').find('input[name="img-position"]').val();
    var req_data = {'img_position': img_data, _token: $('meta[name="csrf-token"]').attr('content')};
    console.log(req_data);
    $.ajax({
        url: url,
        type: type,
        data: req_data,
        success: function (response) {
            alert('Position Updated');
//                                            $(elem).parents(".img-rem").remove();
            // swal(response);
            swal("Poof! Your imaginary file has been updated!", {
                title: "Position Updated",
                icon: "success"
            });
        }
    });
}

</script>
@endpush
