@extends('layouts.app')

@section('contentheader_title')
    EMPLOYEES
@endsection

@section('contentheader_action')
    <a href="{{ route('backend.employee.create') }}" class="btn btn-primary btn-sm">
        <i class="fa fa-plus"></i>
    </a>
@endsection

@section('content')


     <table id="admin-table" class="table table-striped table-bordered table-hover admin-table" style="width:100%">
           <thead class="dark">
            <tr>
                <th>Profile Pic</th>
                <th>Employee ID</th>
                <th>Employee Name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Date of Birth</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>
            @foreach($datas as $data)
                <tr>
                    <td>
                    <img src="{{asset(Config::get('constants.image_dir').'/'.$data->photo)}}" style="width:50px;"></br>
                    </td>
                    <td>{{ $data->unique_id }}</td>
                    <td>{{ $data->name }}</td>
                    <td>{{ $data->mobile }}</td>
                    <td>{{ $data->email }}</td>
                    <td>{{ $data->birth }}</td>
                    <td>
                    <a href="{{ route('backend.employee.edit', ['id' => $data->id]) }}" class="btn btn-success btn-xs">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <!-- <mv-delete url="{{ route('backend.employee.destroy', ['id' => $data->id]) }}" method="delete"></mv-delete> -->
                    <form action="{{ route('backend.employee.destroy', ['id' => $data->id]) }}" method="post" enctype="multipart/form-data" style="display: inline-block;">
                        @csrf
                    <input type="hidden" name="_method" value="DELETE"> 
                    <button type="submit" class="btn btn-danger btn-xs" data-toggle="confirmation">
                    <i class="fa fa-trash"></i>
                    </button>
                    </form>
                  </td>
                </tr>
            @endforeach
            </tbody>

        </table>

@endsection

@push('scripts')

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>
<script>
    $(document).ready(function() {
    $(".admin-table").DataTable({
            "order": [[0, "desc"]],
            "scrollX": true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "dom": 'Bfrtip',
            "buttons": [
                'excel', 'pdf', 'print'
            ]
        });

    $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            placement: 'left',
            onConfirm: function (event, element) {
                element.closest('form').submit();
            }
        });   
});
</script>
@endpush
