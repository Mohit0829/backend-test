@extends('layouts.app')

@section('contentheader_title')
    DASHBOARD
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-3 col-xs-12">
            <div class="small-box bg-gray">
                <div class="inner">
                    <h3>Title</h3>
                    <p>Subtitle</p>
                </div>
                <div class="icon">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <a href="{{ route('backend.dashboard') }}" class="small-box-footer">More info <i
                            class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
@endsection