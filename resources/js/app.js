require('./bootstrap');
window.Vue = require('vue');

// We will import all the javascript library here.
require('./plugins/adminlte');
require('datatables.net');
require('datatables.net-bs');
require('./plugins/icheck.min');
require('./plugins/select2.min');
require('./plugins/select2.min');



// Automatic component resgistration!
Vue.component('mv-alert', require('./components/MvAlertComponent.vue'));
Vue.component('mv-delete', require('./components/MvDeleteComponent.vue'));
Vue.component('mv-input-image', require('./components/MvInputImageComponent.vue'));

const app = new Vue({
    el: '#app',
    mounted() {
        $(".admin-datatable").DataTable({
            "order": [[0, "desc"]],
            "scrollX": true,
            "dom": 'Bfrtip',
            "buttons": [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('.multiple').select2();
        /*$('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /!* optional *!/
        });*/
    }
});
