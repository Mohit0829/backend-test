<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');


Route::namespace('Frontend')->name('frontend.')->group(function () {

     // Route::get('/', 'FrontendController@index')->name('home1');
     // Route::get('/hotshot-originals', 'FrontendController@originalsListing')->name('hotshot-originals');
     // Route::get('/hotshot-x', 'FrontendController@XListing')->name('hotshot-x');
     // Route::get('/hotshot-videos', 'FrontendController@videosListing')->name('hotshot-videos');
     // Route::get('/movie/{slug}', 'FrontendController@movie')->name('movie');
     // Route::get('/gallery', 'FrontendController@gallery')->name('gallery');
     // Route::post('/gallery-form-submit', 'FrontendController@galleryPost')->name('gallery-form-submit');
     // Route::view('/terms-of-use', 'frontend.pages.terms')->name('terms-of-use');
     // Route::view('/privacy-policy', 'frontend.pages.policy')->name('privacy-policy');
     // Route::view('/contest-rules', 'frontend.pages.contest-rules')->name('contest-rules');
});

/*
|--------------------------------------------------------------------------
| Register the typical authentication routes for an application.
| Replacing: Auth::routes();
|--------------------------------------------------------------------------
*/

Route::namespace('Auth')->prefix(config('constants.backend_url_prefix'))->group(function () {
    // Authentication Routes...
    $this->get('login', 'LoginController@showLoginForm')->name('login');
    $this->post('login', 'LoginController@login');
    $this->post('logout', 'LoginController@logout')->name('logout');
    // Registration Routes...
    if (config('constants.register')) {
        $this->get('register', 'RegisterController@showRegistrationForm')->name('register');
        $this->post('register', 'RegisterController@register');
    }
    // Password Reset Routes...
    $this->get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'ResetPasswordController@reset');
});

/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/

Route::namespace('Backend')->prefix(config('constants.backend_url_prefix'))->name('backend.')->middleware(['auth'])->group(function () {

    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    Route::resource('users', 'UserController');
    Route::resource('team', 'TeamController');
    Route::resource('movie', 'MovieController');
    Route::resource('cast', 'CastController');
    Route::resource('video', 'HotVideosController');
    Route::resource('banners', 'BannersController');
    Route::resource('gallery-form', 'GalleryController');
    Route::resource('employee', 'EmployeeController');

    Route::get('movie/deletepic/{id}','MovieController@deletepic');

    Route::post('/movie-gallery-delete/{id}', 'MovieController@deleteGalleryImage')->name('movie-gallery-delete');

    Route::post('/movie-gallery-update/{id}', 'MovieController@updateGalleryImage')->name('movie-gallery-update');

    Route::post('/movie-bts-delete/{id}', 'MovieController@deleteBTSImage')->name('movie-bts-delete');

    Route::post('/movie-bts-update/{id}', 'MovieController@updateBTSImage')->name('movie-bts-update');
});
