<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackendController extends Controller
{
public function __construct()
{
}
public function redirectToIndex($indexPage, $message = '')
{
return redirect()->route("backend.{$indexPage}.index")->with([
'message' => $message
]);
}

public function responseToJson($message = '')
{
return response()->json([
'message' => $message
]);
}
}

