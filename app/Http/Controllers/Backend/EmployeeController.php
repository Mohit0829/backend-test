<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Movie_gallery_image;
use Config;

class EmployeeController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = new Employee();
        $datas=$data->all();
        return view('backend.employee.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datas = Employee::all();
        return view('backend.employee.create')->with(['datas' => $datas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->mcast);
       // $request->validate([
       //      'front_name' => 'required|max:255',
       //      'description' => 'required',
       //      'position' => 'required',
       //  ]);

        // dd($request->all());

        $feature_image = null;
        if ($request->hasFile('feature_image')) {
                $file = $request->file('feature_image');
                $filename = $file->getClientOriginalName();
                $feature_image = date('His').$filename;
                $destinationPath = Config::get('constants.image_dir');
                $file->move($destinationPath, $feature_image);
        }

        $movie = new Employee();
        $movie->photo = $feature_image;
        $movie->name = $request->employee_name;
        $movie->mobile = $request->full_phone;
        $movie->email = $request->email_id;
        $movie->birth = $request->birth;
        $movie->unique_id = $request->employee_id;
        $movie->save();

        return $this->redirectToIndex('employee', config('constants.message.save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Employee::findOrFail($id);

        return view("backend.employee.edit",compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $movie = Employee::Findorfail($id);

        if ($request->hasFile('feature_image')) {
                $file = $request->file('feature_image');
                $filename = $file->getClientOriginalName();
                $feature_image = date('His').$filename;
                $destinationPath = Config::get('constants.image_dir');
                $file->move($destinationPath, $feature_image);

                 $movie->photo = $feature_image;
        }
        $movie->name = $request->employee_name;
        $movie->mobile = $request->full_phone;
        $movie->email = $request->email_id;
        $movie->birth = $request->birth;
        $movie->unique_id = $request->employee_id;
        $movie->save();

         return $this->redirectToIndex('employee', config('constants.message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $movie=Employee::Findorfail($id);
       $movie->delete();
       // return $this->responseToJson(config('constants.message.delete'));
       return $this->redirectToIndex('employee', config('constants.message.delete'));
    }



}
