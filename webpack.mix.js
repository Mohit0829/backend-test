const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const asset = {
    css: "public/backend/css/",
    js: "public/backend/js/",
    image: "public/backend/img/",
    fonts: "public/backend/fonts/",
    dist: "public/backend/dist/"
}

mix.js('resources/js/app.js', asset.js)
    .sass('resources/sass/app.scss', asset.css)
    .copy('node_modules/bootstrap/dist/fonts/*', asset.fonts)
    .options({
        processCssUrls: false
    });

if (mix.inProduction()) {
    mix.version();
}
